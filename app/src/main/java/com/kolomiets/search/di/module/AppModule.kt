package com.kolomiets.search.di.module

import android.content.Context
import android.content.SharedPreferences
import com.kolomiets.search.presentation.router.Router
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val appContext: Context) {

    @Singleton
    @Provides
    fun provideContext() = appContext

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("search_prefs", Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideRouter(context: Context): Router {
        return Router(context)
    }

}
