package com.kolomiets.search.di.component

import com.kolomiets.search.data.di.module.DatabaseModule
import com.kolomiets.search.data.di.module.DatastoreModule
import com.kolomiets.search.data.di.module.NetworkModule
import com.kolomiets.search.data.di.module.RepositoryModule
import com.kolomiets.search.di.module.AppModule
import com.kolomiets.search.presentation.di.module.SearchModule
import com.kolomiets.search.presentation.di.subcomponent.SearchSubComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        DatastoreModule::class,
        RepositoryModule::class
    ]
)
interface AppComponent {
    fun plus(searchModule: SearchModule): SearchSubComponent
}
