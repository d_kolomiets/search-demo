package com.kolomiets.search.data.api.models

import com.google.gson.annotations.SerializedName

class SearchInformationApiModel(
    @SerializedName("searchTime") val searchTime: Double,
    @SerializedName("totalResults") val totalResults: Long
)
