package com.kolomiets.search.data.api.models

import com.google.gson.annotations.SerializedName

class SearchResponseApiModel(
    @SerializedName("searchInformation") val searchInformation: SearchInformationApiModel,
    @SerializedName("items") val items: List<SearchItemApiModel>?
)