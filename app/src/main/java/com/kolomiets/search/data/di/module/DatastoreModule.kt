package com.kolomiets.search.data.di.module

import com.kolomiets.search.data.datasource.LocalDataSource
import com.kolomiets.search.data.datasource.RemoteDataSource
import com.kolomiets.search.data.datasource.contract.SearchDataSource
import dagger.Binds
import dagger.Module

@Module
interface DatastoreModule {

    @Binds
    fun bindRemoteDatastore(remote: RemoteDataSource): SearchDataSource.Remote

    @Binds
    fun bindLocalDatastore(remote: LocalDataSource): SearchDataSource.Local

}