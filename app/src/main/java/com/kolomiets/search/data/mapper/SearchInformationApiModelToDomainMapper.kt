package com.kolomiets.search.data.mapper

import com.kolomiets.search.data.api.models.SearchInformationApiModel
import com.kolomiets.search.data.api.models.SearchItemApiModel
import com.kolomiets.search.data.database.entities.SearchRecordEntity
import com.kolomiets.search.domain.mapper.Mapper
import com.kolomiets.search.domain.model.SearchInformation
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchInformationApiModelToDomainMapper @Inject constructor() : Mapper<SearchInformationApiModel, SearchInformation>() {

    override fun map(from: SearchInformationApiModel) = with(from) {
        SearchInformation(
            searchTime = searchTime,
            totalResults = totalResults
        )
    }

}
