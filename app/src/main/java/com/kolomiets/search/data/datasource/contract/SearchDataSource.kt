package com.kolomiets.search.data.datasource.contract

import com.kolomiets.search.data.api.models.SearchResponseApiModel
import com.kolomiets.search.data.database.entities.SearchRecordEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface SearchDataSource {

    interface Local {
        fun saveResults(entities: List<SearchRecordEntity>): Completable
        fun subscribeForSearchResults(): Flowable<List<SearchRecordEntity>>
        fun clear(): Completable
        fun saveSearchQuery(query: String): Completable
        fun getSavedSearchQuery(): Single<String>
    }

    interface Remote {
        fun search(query: String, limit: Int, startIndex: Int): Single<SearchResponseApiModel>
    }
}
