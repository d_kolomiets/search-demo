package com.kolomiets.search.data.mapper

import com.kolomiets.search.data.database.entities.SearchRecordEntity
import com.kolomiets.search.domain.mapper.Mapper
import com.kolomiets.search.domain.model.SearchRecord
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchEntityToDomainMapper @Inject constructor() : Mapper<SearchRecordEntity, SearchRecord>() {

    override fun map(from: SearchRecordEntity) = with(from) {
        SearchRecord(
            title = title,
            link = link,
            displayLink = displayLink,
            snippet = snippet,
            thumbnailLink = thumbnailLink
        )
    }

    override fun reverse(source: SearchRecord) = with(source) {
        SearchRecordEntity(
            title = title,
            link = link,
            displayLink = displayLink,
            snippet = snippet,
            thumbnailLink = thumbnailLink
        )
    }
}
