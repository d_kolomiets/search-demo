package com.kolomiets.search.data.di.module

import com.kolomiets.search.data.repository.SearchRepositoryImpl
import com.kolomiets.search.domain.repository.SearchRepository
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {

    @Binds
    fun bindRepository(repository: SearchRepositoryImpl): SearchRepository

}
