package com.kolomiets.search.data.mapper

import com.kolomiets.search.data.api.models.SearchItemApiModel
import com.kolomiets.search.data.database.entities.SearchRecordEntity
import com.kolomiets.search.domain.mapper.Mapper
import com.kolomiets.search.domain.model.SearchRecord
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchApiModelToDomainMapper @Inject constructor() : Mapper<SearchItemApiModel, SearchRecord>() {

    override fun map(from: SearchItemApiModel) = with(from) {
        SearchRecord(
            title = title,
            link = link,
            displayLink = displayLink,
            snippet = snippet,
            thumbnailLink = pagemap?.image?.firstOrNull()?.thumbnailLink.orEmpty()
        )
    }

}
