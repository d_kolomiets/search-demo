package com.kolomiets.search.data.api

import com.kolomiets.search.data.api.models.SearchResponseApiModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface GoogleSearchApi {

    @GET(".")
    fun search(
        @Query("q") query: String,
        @Query("num") limit: Int,
        @Query("start") startIndex: Int
    ): Single<SearchResponseApiModel>

}
