package com.kolomiets.search.data.di.module

import com.kolomiets.search.BuildConfig
import com.kolomiets.search.data.api.GoogleSearchApi
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Singleton

@Module
class NetworkModule(
    private val baseUrl: String,
    private val apiKey: String,
    private val engineId: String,
    private val customFields: String
) {

    @Singleton
    @Provides
    fun provideInterceptors(): ArrayList<Interceptor> {
        val loggingInterceptor = HttpLoggingInterceptor(
            HttpLoggingInterceptor.Logger { message -> Timber.d(message) }
        )
        loggingInterceptor.level = when {
            BuildConfig.DEBUG -> HttpLoggingInterceptor.Level.BODY
            else -> HttpLoggingInterceptor.Level.NONE
        }

        val searchEngineInterceptor = Interceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()
            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("key", apiKey)
                .addQueryParameter("cx", engineId)
                .addQueryParameter("fields", customFields)
                .build()

            val request = original.newBuilder()
                .url(url)
                .build()

            chain.proceed(request)
        }

        return arrayListOf(
            loggingInterceptor,
            searchEngineInterceptor
        )
    }

    @Singleton
    @Provides
    fun provideRetrofit(interceptors: ArrayList<Interceptor>): Retrofit {
        val clientBuilder = OkHttpClient.Builder()
        interceptors.forEach { interceptor ->
            clientBuilder.addInterceptor(interceptor)
        }
        return Retrofit.Builder()
            .client(clientBuilder.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build()
    }

    @Singleton
    @Provides
    fun provideApi(retrofit: Retrofit): GoogleSearchApi {
        return retrofit.create(GoogleSearchApi::class.java)
    }

}
