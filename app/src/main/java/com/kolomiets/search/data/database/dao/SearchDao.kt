package com.kolomiets.search.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kolomiets.search.data.database.entities.SearchRecordEntity
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface SearchDao {

    @Query("SELECT * FROM search_records")
    fun subscribeResults(): Flowable<List<SearchRecordEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun putSearchResults(records: List<SearchRecordEntity>): Completable

    @Query("DELETE FROM search_records")
    fun clearResults(): Completable

}
