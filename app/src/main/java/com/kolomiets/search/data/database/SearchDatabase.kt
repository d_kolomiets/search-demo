package com.kolomiets.search.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.kolomiets.search.data.database.dao.SearchDao
import com.kolomiets.search.data.database.entities.SearchRecordEntity

@Database(
    entities = [SearchRecordEntity::class],
    version = 1,
    exportSchema = false
)
abstract class SearchDatabase : RoomDatabase() {
    abstract fun searchDao(): SearchDao
}
