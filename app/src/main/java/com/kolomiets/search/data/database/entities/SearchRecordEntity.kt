package com.kolomiets.search.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "search_records")
class SearchRecordEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Long = 0,
    @ColumnInfo(name = "title") val title : String,
    @ColumnInfo(name = "link") val link : String,
    @ColumnInfo(name = "display_link") val displayLink: String,
    @ColumnInfo(name = "snippet") val snippet : String,
    @ColumnInfo(name = "thumbnail_link") val thumbnailLink : String
)
