package com.kolomiets.search.data.datasource

import com.kolomiets.search.data.api.GoogleSearchApi
import com.kolomiets.search.data.api.models.SearchResponseApiModel
import com.kolomiets.search.data.datasource.contract.SearchDataSource
import io.reactivex.Single
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val api: GoogleSearchApi
) : SearchDataSource.Remote {

    override fun search(query: String, limit: Int, startIndex: Int): Single<SearchResponseApiModel> {
        return api.search(query, limit, startIndex)
    }

}
