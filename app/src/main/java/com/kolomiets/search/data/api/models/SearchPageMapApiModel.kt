package com.kolomiets.search.data.api.models

import com.google.gson.annotations.SerializedName

class SearchPageMapApiModel(
    @SerializedName("cse_thumbnail") val image : List<SearchImageApiModel>
)
