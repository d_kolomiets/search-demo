package com.kolomiets.search.data.di.module

import android.content.Context
import androidx.room.Room
import com.kolomiets.search.data.database.SearchDatabase
import com.kolomiets.search.data.database.dao.SearchDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(context: Context): SearchDatabase {
        return Room.databaseBuilder(context, SearchDatabase::class.java, "search_db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideSearchDao(database: SearchDatabase): SearchDao {
        return database.searchDao()
    }

}
