package com.kolomiets.search.data.api.models

import com.google.gson.annotations.SerializedName

class SearchImageApiModel(
    @SerializedName("src") val thumbnailLink: String
)
