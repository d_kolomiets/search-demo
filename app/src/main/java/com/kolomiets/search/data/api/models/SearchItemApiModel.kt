package com.kolomiets.search.data.api.models

import com.google.gson.annotations.SerializedName

class SearchItemApiModel(
    @SerializedName("title") val title : String,
    @SerializedName("link") val link : String,
    @SerializedName("displayLink") val displayLink : String,
    @SerializedName("snippet") val snippet : String,
    @SerializedName("pagemap") val pagemap : SearchPageMapApiModel?
)
