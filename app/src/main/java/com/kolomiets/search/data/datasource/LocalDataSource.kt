package com.kolomiets.search.data.datasource

import android.content.SharedPreferences
import androidx.core.content.edit
import com.kolomiets.search.data.database.dao.SearchDao
import com.kolomiets.search.data.database.entities.SearchRecordEntity
import com.kolomiets.search.data.datasource.contract.SearchDataSource
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class LocalDataSource @Inject constructor(
    private val searchDao: SearchDao,
    private val sharedPreferences: SharedPreferences
): SearchDataSource.Local {

    override fun subscribeForSearchResults(): Flowable<List<SearchRecordEntity>> {
        return searchDao.subscribeResults()
    }

    override fun saveResults(entities: List<SearchRecordEntity>): Completable {
        return searchDao.putSearchResults(entities)
    }

    override fun clear(): Completable {
        return searchDao.clearResults()
    }

    override fun saveSearchQuery(query: String): Completable {
        return Completable.fromAction {
            sharedPreferences.edit {
                putString("search_query", query)
            }
        }
    }

    override fun getSavedSearchQuery(): Single<String> {
        return Single.fromCallable {
            sharedPreferences.getString("search_query", "")
        }
    }

}
