package com.kolomiets.search.data.repository

import com.kolomiets.search.data.datasource.contract.SearchDataSource
import com.kolomiets.search.data.mapper.SearchApiModelToDomainMapper
import com.kolomiets.search.data.mapper.SearchEntityToDomainMapper
import com.kolomiets.search.data.mapper.SearchInformationApiModelToDomainMapper
import com.kolomiets.search.domain.model.SearchInformation
import com.kolomiets.search.domain.model.SearchRecord
import com.kolomiets.search.domain.repository.SearchRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class SearchRepositoryImpl @Inject constructor(
    private val remoteDataSource: SearchDataSource.Remote,
    private val localDataSource: SearchDataSource.Local,
    private val searchApiModelToDomainMapper: SearchApiModelToDomainMapper,
    private val searchEntityToDomainMapper: SearchEntityToDomainMapper,
    private val searchInformationApiModelToDomainMapper: SearchInformationApiModelToDomainMapper
) : SearchRepository {

    override fun search(query: String, limit: Int, startIndex: Int): Single<Pair<SearchInformation, List<SearchRecord>>> {
        return remoteDataSource.search(query, limit, startIndex).map {
            val searchInformation = searchInformationApiModelToDomainMapper.map(it.searchInformation)
            val searchRecords = searchApiModelToDomainMapper.map(it.items ?: emptyList())
            searchInformation to searchRecords
        }
    }

    override fun subscribeForSearchResults(): Flowable<List<SearchRecord>> {
        return localDataSource.subscribeForSearchResults().map {
            searchEntityToDomainMapper.map(it)
        }
    }

    override fun saveResults(results: List<SearchRecord>): Completable {
        return localDataSource.saveResults(searchEntityToDomainMapper.reverse(results))
    }

    override fun clear(): Completable {
        return localDataSource.clear()
    }

    override fun saveSearchQuery(query: String): Completable {
        return localDataSource.saveSearchQuery(query)
    }

    override fun getSavedSearchQuery(): Single<String> {
        return localDataSource.getSavedSearchQuery()
    }

}
