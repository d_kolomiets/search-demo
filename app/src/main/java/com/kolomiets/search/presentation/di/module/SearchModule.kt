package com.kolomiets.search.presentation.di.module

import com.kolomiets.search.domain.repository.SearchRepository
import com.kolomiets.search.domain.usecases.GetSearchQueryUseCase
import com.kolomiets.search.domain.usecases.GetSearchResultsUseCase
import com.kolomiets.search.domain.usecases.SaveSearchQueryUseCase
import com.kolomiets.search.domain.usecases.SubscribeSearchResultsUseCase
import com.kolomiets.search.presentation.common.transformers.AsyncCompletableTransformer
import com.kolomiets.search.presentation.common.transformers.AsyncFlowableTransformer
import com.kolomiets.search.presentation.common.transformers.AsyncSingleTransformer
import com.kolomiets.search.presentation.common.transformers.FixedThreadCompletableTransformer
import com.kolomiets.search.presentation.di.SearchScope
import com.kolomiets.search.presentation.mapper.SearchRecordViewModelMapper
import com.kolomiets.search.presentation.viewmodel.factory.SearchViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class SearchModule {

    @SearchScope
    @Provides
    fun provideSearchViewModelFactory(
        getSearchResultsUseCase: GetSearchResultsUseCase,
        subscribeSearchResultsUseCase: SubscribeSearchResultsUseCase,
        saveSearchQueryUseCase: SaveSearchQueryUseCase,
        getSearchQueryUseCase: GetSearchQueryUseCase,
        searchRecordViewModelMapper: SearchRecordViewModelMapper
    ): SearchViewModelFactory {
        return SearchViewModelFactory(
            getSearchResultsUseCase = getSearchResultsUseCase,
            subscribeSearchResultsUseCase = subscribeSearchResultsUseCase,
            saveSearchQueryUseCase = saveSearchQueryUseCase,
            getSearchQueryUseCase = getSearchQueryUseCase,
            mapper = searchRecordViewModelMapper
        )
    }

    @SearchScope
    @Provides
    fun provideSearchUseCase(searchRepository: SearchRepository): GetSearchResultsUseCase {
        return GetSearchResultsUseCase(AsyncCompletableTransformer(), searchRepository)
    }

    @SearchScope
    @Provides
    fun provideSubscribeSearchResultsUseCase(searchRepository: SearchRepository): SubscribeSearchResultsUseCase {
        return SubscribeSearchResultsUseCase(AsyncFlowableTransformer(), searchRepository)
    }

    @SearchScope
    @Provides
    fun provideSaveSearchQueryUseCase(searchRepository: SearchRepository): SaveSearchQueryUseCase {
        return SaveSearchQueryUseCase(AsyncCompletableTransformer(), searchRepository)
    }

    @SearchScope
    @Provides
    fun provideGetSearchQueryUseCase(searchRepository: SearchRepository): GetSearchQueryUseCase {
        return GetSearchQueryUseCase(AsyncSingleTransformer(), searchRepository)
    }

}
