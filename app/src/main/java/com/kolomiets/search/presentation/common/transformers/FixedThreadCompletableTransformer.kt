package com.kolomiets.search.presentation.common.transformers

import com.kolomiets.search.domain.usecases.base.transformer.TransformerCompletable
import io.reactivex.Completable
import io.reactivex.CompletableSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors

class FixedThreadCompletableTransformer(
    private val numberOfThreads: Int
) : TransformerCompletable() {

    override fun apply(upstream: Completable): CompletableSource {
        return upstream.subscribeOn(Schedulers.from(Executors.newFixedThreadPool(numberOfThreads)))
            .observeOn(AndroidSchedulers.mainThread())
    }
}
