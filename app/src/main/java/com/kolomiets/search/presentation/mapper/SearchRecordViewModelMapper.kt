package com.kolomiets.search.presentation.mapper

import com.kolomiets.search.domain.mapper.Mapper
import com.kolomiets.search.domain.model.SearchRecord
import com.kolomiets.search.presentation.model.SearchRecordViewModel
import javax.inject.Inject

class SearchRecordViewModelMapper @Inject constructor(): Mapper<SearchRecord, SearchRecordViewModel>() {

    override fun map(from: SearchRecord) = with(from) {
        SearchRecordViewModel(
            title = title,
            link = link,
            displayLink = displayLink,
            snippet = snippet,
            thumbnailLink = thumbnailLink
        )
    }

}
