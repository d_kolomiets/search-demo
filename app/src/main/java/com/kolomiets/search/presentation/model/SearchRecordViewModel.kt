package com.kolomiets.search.presentation.model

data class SearchRecordViewModel(
    val title : String,
    val link : String,
    val displayLink: String,
    val snippet : String,
    val thumbnailLink : String
)