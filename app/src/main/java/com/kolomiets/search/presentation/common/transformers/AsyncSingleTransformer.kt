package com.kolomiets.search.presentation.common.transformers

import com.kolomiets.search.domain.usecases.base.transformer.TransformerSingle
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AsyncSingleTransformer<T> : TransformerSingle<T>() {
    override fun apply(upstream: Single<T>): SingleSource<T> {
        return upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}
