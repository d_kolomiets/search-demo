package com.kolomiets.search.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.kolomiets.search.App
import com.kolomiets.search.R
import com.kolomiets.search.presentation.adapter.SearchAdapter
import com.kolomiets.search.presentation.router.Router
import com.kolomiets.search.presentation.types.SearchError
import com.kolomiets.search.presentation.viewmodel.SearchViewModel
import com.kolomiets.search.presentation.viewmodel.factory.SearchViewModelFactory
import kotlinx.android.synthetic.main.f_search.*
import javax.inject.Inject
import javax.inject.Qualifier

class SearchFragment : Fragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    @Inject
    lateinit var factory: SearchViewModelFactory

    @Inject
    lateinit var router: Router

    private lateinit var viewModel: SearchViewModel

    private val searchAdapter = SearchAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        App.self.provideSearchComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.f_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, factory).get(SearchViewModel::class.java)

        resultsListView.adapter = searchAdapter
        searchAdapter.itemClickListener = { searchRecord ->
            viewModel.onSearchResultClicked(searchRecord)
        }

        subscribeViewModel(viewModel)

        searchButtonView.setOnClickListener {
            val query = searchInputView.text.toString()
            viewModel.onSearchClicked(query)
        }
        stopSearchButtonView.setOnClickListener {
            viewModel.onStopSearchClicked()
        }
    }

    private fun subscribeViewModel(viewModel: SearchViewModel) {
        with(viewModel) {
            openLink.observe(viewLifecycleOwner, Observer { router.goToUrl(it) })
            errorLiveData.observe(viewLifecycleOwner, Observer { showError(it) })
            lastQuery.observe(viewLifecycleOwner, Observer { searchInputView.setText(it) })
            results.observe(viewLifecycleOwner, Observer { searchAdapter.updateResults(it) })
            searchButtonAvailable.observe(viewLifecycleOwner, Observer { searchButtonView.isEnabled = it })
            stopButtonAvailable.observe(viewLifecycleOwner, Observer { stopSearchButtonView.isEnabled = it })
        }
    }

    private fun showError(error: SearchError) {
        val message = when(error) {
            SearchError.QueryIsEmpty -> getString(R.string.error_empty_query)
            SearchError.EmptyResult -> getString(R.string.error_nothing_found)
            is SearchError.UnknownError -> error.message
        }
        view?.let { Snackbar.make(it, message, Snackbar.LENGTH_SHORT).show() }
    }

}
