package com.kolomiets.search.presentation.common.transformers

import com.kolomiets.search.domain.usecases.base.transformer.TransformerCompletable
import io.reactivex.Completable
import io.reactivex.CompletableSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AsyncCompletableTransformer : TransformerCompletable() {
    override fun apply(upstream: Completable): CompletableSource {
        return upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}
