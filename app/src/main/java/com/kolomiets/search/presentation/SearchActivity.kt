package com.kolomiets.search.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kolomiets.search.R
import com.kolomiets.search.presentation.fragment.SearchFragment

class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_search)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, SearchFragment.newInstance())
                    .commitNow()
        }
    }

}
