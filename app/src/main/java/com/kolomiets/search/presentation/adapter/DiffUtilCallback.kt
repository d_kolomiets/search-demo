package com.kolomiets.search.presentation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.kolomiets.search.presentation.model.SearchRecordViewModel

class DiffUtilCallback(
    private val oldList: List<SearchRecordViewModel>,
    private val newList: List<SearchRecordViewModel>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}
