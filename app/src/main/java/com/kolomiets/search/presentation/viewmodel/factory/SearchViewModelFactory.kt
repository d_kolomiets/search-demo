package com.kolomiets.search.presentation.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kolomiets.search.domain.usecases.GetSearchQueryUseCase
import com.kolomiets.search.domain.usecases.GetSearchResultsUseCase
import com.kolomiets.search.domain.usecases.SaveSearchQueryUseCase
import com.kolomiets.search.domain.usecases.SubscribeSearchResultsUseCase
import com.kolomiets.search.presentation.mapper.SearchRecordViewModelMapper
import com.kolomiets.search.presentation.viewmodel.SearchViewModel

class SearchViewModelFactory(
    private val getSearchResultsUseCase: GetSearchResultsUseCase,
    private val subscribeSearchResultsUseCase: SubscribeSearchResultsUseCase,
    private val saveSearchQueryUseCase: SaveSearchQueryUseCase,
    private val getSearchQueryUseCase: GetSearchQueryUseCase,
    private val mapper: SearchRecordViewModelMapper
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchViewModel(
            getSearchResultsUseCase,
            subscribeSearchResultsUseCase,
            saveSearchQueryUseCase,
            getSearchQueryUseCase,
            mapper
        ) as T
    }

}
