package com.kolomiets.search.presentation.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

inline fun <reified T : View> ViewGroup.inflateChild(@LayoutRes resId: Int, attachToRoot: Boolean = false): T {
    return LayoutInflater.from(context).inflate(resId, this, attachToRoot) as T
}
