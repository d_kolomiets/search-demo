package com.kolomiets.search.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kolomiets.search.domain.error.NoResultsFoundError
import com.kolomiets.search.domain.usecases.GetSearchQueryUseCase
import com.kolomiets.search.domain.usecases.GetSearchResultsUseCase
import com.kolomiets.search.domain.usecases.SaveSearchQueryUseCase
import com.kolomiets.search.domain.usecases.SubscribeSearchResultsUseCase
import com.kolomiets.search.presentation.common.SingleLiveEvent
import com.kolomiets.search.presentation.mapper.SearchRecordViewModelMapper
import com.kolomiets.search.presentation.model.SearchRecordViewModel
import com.kolomiets.search.presentation.types.SearchError
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

class SearchViewModel(
    private val getSearchResultsUseCase: GetSearchResultsUseCase,
    private val subscribeSearchResultsUseCase: SubscribeSearchResultsUseCase,
    private val saveSearchQueryUseCase: SaveSearchQueryUseCase,
    private val getSearchQueryUseCase: GetSearchQueryUseCase,
    private val mapper: SearchRecordViewModelMapper
) : ViewModel() {

    private val subscriptions = CompositeDisposable()
    private var searchSubscription: Disposable? = null

    private val _errorLiveData = SingleLiveEvent<SearchError>()
    private val _results = MutableLiveData<List<SearchRecordViewModel>>()
    private val _openLink = SingleLiveEvent<String>()
    private val _searchButtonAvailable = MutableLiveData<Boolean>().apply { value = true }
    private val _stopButtonAvailable = MutableLiveData<Boolean>().apply { value = false }
    private val _lastQuery = SingleLiveEvent<String>()

    val results: LiveData<List<SearchRecordViewModel>>
        get() = _results
    val openLink: LiveData<String>
        get() = _openLink
    val searchButtonAvailable: LiveData<Boolean>
        get() = _searchButtonAvailable
    val stopButtonAvailable: LiveData<Boolean>
        get() = _stopButtonAvailable
    val errorLiveData: LiveData<SearchError>
        get() = _errorLiveData
    val lastQuery: LiveData<String>
        get() = _lastQuery

    init {
        subscriptions += subscribeSearchResultsUseCase.execute(
            params = SubscribeSearchResultsUseCase.Params(),
            onNewData = { showResults(mapper.map(it)) },
            onFailure = { showError(SearchError.UnknownError(it.localizedMessage)) }
        )
        subscriptions += getSearchQueryUseCase.execute(
            params = GetSearchQueryUseCase.Params(),
            onSuccess = { _lastQuery.value = it }
        )
    }

    override fun onCleared() {
        super.onCleared()

        subscriptions.clear()
        searchSubscription?.dispose()
    }

    fun onSearchClicked(query: String) {
        if (query.isBlank()) {
            _errorLiveData.value = SearchError.QueryIsEmpty
            return
        }
        applySearchingState()
        searchSubscription = getSearchResultsUseCase.execute(
            params = GetSearchResultsUseCase.Params(query, SEARCH_RESULTS_LIMIT),
            onSuccess = {
                _lastQuery.value = query
                saveSearchQuery(query)
                applyIdleState()
            },
            onFailure = { error ->
                when (error) {
                    is NoResultsFoundError -> showError(SearchError.EmptyResult)
                    else -> {
                        Timber.e(error)
                        showError(SearchError.UnknownError(error.localizedMessage))
                    }
                }
                applyIdleState()
            }
        )
    }

    fun onStopSearchClicked() {
        applyIdleState()
        searchSubscription?.dispose()
    }

    fun onSearchResultClicked(searchRecord: SearchRecordViewModel) {
        _openLink.value = searchRecord.link
    }

    private fun applyIdleState() {
        _searchButtonAvailable.value = true
        _stopButtonAvailable.value = false
    }

    private fun applySearchingState() {
        _searchButtonAvailable.value = false
        _stopButtonAvailable.value = true
    }

    private fun saveSearchQuery(query: String) {
        subscriptions += saveSearchQueryUseCase.execute(
            params = SaveSearchQueryUseCase.Params(query)
        )
    }

    private fun showError(error: SearchError) {
        _errorLiveData.value = error
    }

    private fun showResults(results: List<SearchRecordViewModel>) {
        _results.value = results
    }

    companion object {
        private const val SEARCH_RESULTS_LIMIT = 30
    }

}
