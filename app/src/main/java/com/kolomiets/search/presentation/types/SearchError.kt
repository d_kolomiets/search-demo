package com.kolomiets.search.presentation.types

sealed class SearchError {
    object QueryIsEmpty : SearchError()
    object EmptyResult : SearchError()
    class UnknownError(val message: String) : SearchError()
}
