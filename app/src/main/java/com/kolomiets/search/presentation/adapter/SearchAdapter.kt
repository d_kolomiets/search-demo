package com.kolomiets.search.presentation.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kolomiets.search.R
import com.kolomiets.search.presentation.extensions.inflateChild
import com.kolomiets.search.presentation.model.SearchRecordViewModel
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.v_search_record.view.*
import timber.log.Timber


class SearchAdapter : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    private var data: List<SearchRecordViewModel> = emptyList()

    var itemClickListener: ((SearchRecordViewModel) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflateChild(R.layout.v_search_record), itemClickListener)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.update(data[position])
    }

    fun updateResults(results: List<SearchRecordViewModel>) {
        Timber.d("updateResults: ${results.map { it.link }}")
        val diffResult = DiffUtil.calculateDiff(DiffUtilCallback(data, results))
        data = results
        diffResult.dispatchUpdatesTo(this)
    }

    class ViewHolder(
        override val containerView: View,
        private val listener: ((SearchRecordViewModel) -> Unit)?
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun update(model: SearchRecordViewModel) {
            with(containerView) {
                titleView.text = model.title
                summaryView.text = model.snippet
                urlView.text = model.displayLink
                Glide.with(thumbnailView)
                    .load(model.thumbnailLink)
                    .optionalCenterCrop()
                    .into(thumbnailView)
                setOnClickListener { listener?.invoke(model) }
            }
        }
    }

}