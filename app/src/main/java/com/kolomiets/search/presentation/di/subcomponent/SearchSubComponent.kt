package com.kolomiets.search.presentation.di.subcomponent

import com.kolomiets.search.presentation.di.SearchScope
import com.kolomiets.search.presentation.di.module.SearchModule
import com.kolomiets.search.presentation.fragment.SearchFragment
import dagger.Subcomponent

@SearchScope
@Subcomponent(
    modules = [SearchModule::class]
)
interface SearchSubComponent {
    fun inject(fragment: SearchFragment)
}
