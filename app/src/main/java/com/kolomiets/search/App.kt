package com.kolomiets.search

import android.app.Application
import com.kolomiets.search.data.di.module.DatabaseModule
import com.kolomiets.search.data.di.module.NetworkModule
import com.kolomiets.search.di.component.AppComponent
import com.kolomiets.search.di.component.DaggerAppComponent
import com.kolomiets.search.di.module.AppModule
import com.kolomiets.search.presentation.di.module.SearchModule
import com.kolomiets.search.presentation.di.subcomponent.SearchSubComponent
import timber.log.Timber

class App : Application() {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        self = this

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        initDependencies()
    }

    private fun initDependencies() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(applicationContext))
            .networkModule(
                NetworkModule(
                    baseUrl = getString(R.string.search_api_url),
                    apiKey = getString(R.string.search_api_key),
                    engineId = getString(R.string.search_api_engine_id),
                    customFields = getString(R.string.search_api_custom_fields)
                )
            )
            .databaseModule(DatabaseModule())
            .build()
    }

    fun provideSearchComponent(): SearchSubComponent {
        return appComponent.plus(SearchModule())
    }

    companion object {
        lateinit var self: App
    }

}
