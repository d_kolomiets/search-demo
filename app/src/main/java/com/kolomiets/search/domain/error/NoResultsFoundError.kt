package com.kolomiets.search.domain.error

import java.lang.Exception

class NoResultsFoundError : Exception()