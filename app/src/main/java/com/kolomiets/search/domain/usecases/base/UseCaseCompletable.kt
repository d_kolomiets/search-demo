package com.kolomiets.search.domain.usecases.base

import com.kolomiets.search.domain.usecases.base.transformer.TransformerCompletable
import io.reactivex.Completable
import io.reactivex.disposables.Disposable


abstract class UseCaseCompletable<P>(
    private val transformer: TransformerCompletable
) {

    abstract fun createObservable(params: P): Completable

    fun execute(
        params: P,
        onSuccess: () -> Unit = {},
        onFailure: (error: Throwable) -> Unit = {}
    ): Disposable {
        return createObservable(params)
            .compose(transformer)
            .subscribe(
                { onSuccess.invoke() },
                { onFailure.invoke(it) }
            )
    }

}
