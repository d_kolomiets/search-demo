package com.kolomiets.search.domain.model

data class SearchRecord(
    val title : String,
    val link : String,
    val displayLink: String,
    val snippet : String,
    val thumbnailLink : String
)
