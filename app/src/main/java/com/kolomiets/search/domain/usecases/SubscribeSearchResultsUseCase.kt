package com.kolomiets.search.domain.usecases

import com.kolomiets.search.domain.model.SearchRecord
import com.kolomiets.search.domain.repository.SearchRepository
import com.kolomiets.search.domain.usecases.base.transformer.TransformerCompletable
import com.kolomiets.search.domain.usecases.base.UseCaseCompletable
import com.kolomiets.search.domain.usecases.base.UseCaseFlowable
import com.kolomiets.search.domain.usecases.base.transformer.TransformerFlowable
import io.reactivex.Completable
import io.reactivex.Flowable

class SubscribeSearchResultsUseCase(
    transformer: TransformerFlowable<List<SearchRecord>>,
    private val searchRepository: SearchRepository
) : UseCaseFlowable<List<SearchRecord>, SubscribeSearchResultsUseCase.Params>(transformer) {

    override fun createObservable(data: Params): Flowable<List<SearchRecord>> {
        return searchRepository.subscribeForSearchResults()
    }

    class Params
}
