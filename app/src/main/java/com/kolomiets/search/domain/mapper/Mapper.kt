package com.kolomiets.search.domain.mapper

import java.lang.UnsupportedOperationException

abstract class Mapper<From, To> {

    abstract fun map(from: From): To

    fun map(listFrom: List<From>): List<To> {
        return listFrom.map { map(it) }
    }

    open fun reverse(source: To): From {
        throw UnsupportedOperationException("Override this method in your mapper for use")
    }

    fun reverse(listFrom: List<To>): List<From> {
        return listFrom.map { reverse(it) }
    }

}
