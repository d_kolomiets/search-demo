package com.kolomiets.search.domain.model

class SearchInformation(
    val searchTime: Double,
    val totalResults: Long
)
