package com.kolomiets.search.domain.usecases.base.transformer

import io.reactivex.CompletableTransformer

abstract class TransformerCompletable : CompletableTransformer
