package com.kolomiets.search.domain.usecases.base.transformer

import io.reactivex.FlowableTransformer

abstract class TransformerFlowable<T> : FlowableTransformer<T, T>
