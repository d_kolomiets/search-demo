package com.kolomiets.search.domain.repository

import com.kolomiets.search.domain.model.SearchInformation
import com.kolomiets.search.domain.model.SearchRecord
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface SearchRepository {

    fun search(query: String, limit: Int, startIndex: Int): Single<Pair<SearchInformation, List<SearchRecord>>>

    fun subscribeForSearchResults(): Flowable<List<SearchRecord>>

    fun saveResults(results: List<SearchRecord>): Completable

    fun clear(): Completable

    fun saveSearchQuery(query: String): Completable

    fun getSavedSearchQuery(): Single<String>

}
