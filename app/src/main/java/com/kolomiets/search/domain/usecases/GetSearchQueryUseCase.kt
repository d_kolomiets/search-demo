package com.kolomiets.search.domain.usecases

import com.kolomiets.search.domain.repository.SearchRepository
import com.kolomiets.search.domain.usecases.base.UseCaseSingle
import com.kolomiets.search.domain.usecases.base.transformer.TransformerSingle
import io.reactivex.Single

class GetSearchQueryUseCase(
    transformer: TransformerSingle<String>,
    private val searchRepository: SearchRepository
) : UseCaseSingle<String, GetSearchQueryUseCase.Params>(transformer) {

    override fun createObservable(params: Params): Single<String> {
        return searchRepository.getSavedSearchQuery()
    }

    class Params

}
