package com.kolomiets.search.domain.usecases

import com.kolomiets.search.domain.error.NoResultsFoundError
import com.kolomiets.search.domain.model.SearchInformation
import com.kolomiets.search.domain.model.SearchRecord
import com.kolomiets.search.domain.repository.SearchRepository
import com.kolomiets.search.domain.usecases.base.transformer.TransformerCompletable
import com.kolomiets.search.domain.usecases.base.UseCaseCompletable
import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors

class GetSearchResultsUseCase(
    transformer: TransformerCompletable,
    private val searchRepository: SearchRepository
) : UseCaseCompletable<GetSearchResultsUseCase.Params>(transformer) {

    private var schedulerIndex = 0
    private val schedulers = arrayOf(
        Schedulers.from(Executors.newSingleThreadExecutor()),
        Schedulers.from(Executors.newSingleThreadExecutor())
    )

    override fun createObservable(params: Params): Completable {
        var startIndex = 1
        return searchRepository.clear().andThen(
            searchRepository.search(params.query, SEARCH_PAGE_SIZE, startIndex).subscribeOn(resolveScheduler())
                .flatMapCompletable { (info, results) ->
                    if (info.totalResults == 0L) {
                        return@flatMapCompletable Completable.error(NoResultsFoundError())
                    }
                    var actualSearchLimit = params.resultsLimit
                    if (info.totalResults < actualSearchLimit) {
                        actualSearchLimit = info.totalResults.toInt()
                    }
                    val fullPagesToLoad = actualSearchLimit / SEARCH_PAGE_SIZE
                    val extraPage = actualSearchLimit % SEARCH_PAGE_SIZE

                    val observables = mutableListOf<Single<Pair<SearchInformation, List<SearchRecord>>>>()
                    startIndex += SEARCH_PAGE_SIZE
                    for (i in 1 until fullPagesToLoad) { // offset = SEARCH_PAGE_SIZE and 1 as initial are because we have already received first page
                        observables.add(searchRepository.search(params.query, SEARCH_PAGE_SIZE, startIndex).subscribeOn(resolveScheduler()))
                        startIndex += SEARCH_PAGE_SIZE
                    }
                    if (extraPage > 0) {
                        observables.add(searchRepository.search(params.query, extraPage, startIndex).subscribeOn(resolveScheduler()))
                    }
                    searchRepository.saveResults(results).andThen(
                        Single.merge<Pair<SearchInformation, List<SearchRecord>>>(observables)
                            .flatMapCompletable { (_, results) ->
                                searchRepository.saveResults(results)
                            }
                    )
                }
        )
    }

    private fun resolveScheduler(): Scheduler {
        return schedulers[schedulerIndex].also {
            schedulerIndex++
            if (schedulerIndex >= schedulers.size) {
                schedulerIndex = 0
            }
        }
    }

    class Params(
        val query: String,
        val resultsLimit: Int
    )

    companion object {
        private const val SEARCH_PAGE_SIZE = 5
    }
}
