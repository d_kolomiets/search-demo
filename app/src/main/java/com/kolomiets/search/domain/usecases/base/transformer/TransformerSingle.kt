package com.kolomiets.search.domain.usecases.base.transformer

import io.reactivex.SingleTransformer

abstract class TransformerSingle<T> : SingleTransformer<T, T>
