package com.kolomiets.search.domain.usecases

import com.kolomiets.search.domain.repository.SearchRepository
import com.kolomiets.search.domain.usecases.base.transformer.TransformerCompletable
import com.kolomiets.search.domain.usecases.base.UseCaseCompletable
import io.reactivex.Completable

class SaveSearchQueryUseCase(
    transformer: TransformerCompletable,
    private val searchRepository: SearchRepository
) : UseCaseCompletable<SaveSearchQueryUseCase.Params>(transformer) {

    override fun createObservable(params: Params): Completable {
        return searchRepository.saveSearchQuery(params.query)
    }

    class Params(
        val query: String
    )
}
