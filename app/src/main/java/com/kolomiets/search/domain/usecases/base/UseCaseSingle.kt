package com.kolomiets.search.domain.usecases.base

import com.kolomiets.search.domain.usecases.base.transformer.TransformerSingle
import io.reactivex.Single
import io.reactivex.disposables.Disposable


abstract class UseCaseSingle<T, P>(
    private val transformer: TransformerSingle<T>
) {

    abstract fun createObservable(params: P): Single<T>

    fun execute(
        params: P,
        onSuccess: (T) -> Unit = {},
        onFailure: (error: Throwable) -> Unit = {}
    ): Disposable {
        return createObservable(params)
            .compose(transformer)
            .subscribe(
                { onSuccess.invoke(it) },
                { onFailure.invoke(it) }
            )
    }

}
