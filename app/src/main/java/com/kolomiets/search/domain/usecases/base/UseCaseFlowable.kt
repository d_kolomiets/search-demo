package com.kolomiets.search.domain.usecases.base

import com.kolomiets.search.domain.usecases.base.transformer.TransformerFlowable
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable


abstract class UseCaseFlowable<T, P>(
    private val transformer: TransformerFlowable<T>
) {

    abstract fun createObservable(params: P): Flowable<T>

    fun execute(
        params: P,
        onNewData: (T) -> Unit,
        onFailure: (error: Throwable) -> Unit
    ): Disposable {
        return createObservable(params)
            .compose(transformer)
            .subscribe(
                { onNewData.invoke(it) },
                { onFailure.invoke(it) }
            )
    }

}
